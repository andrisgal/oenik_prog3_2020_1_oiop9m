﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Felsookt.Program
{
    using Felsookt.Data;
    using Felsookt.Logic;

    /// <summary>
    /// this is the program, where everything is done.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// this is used for checking whether the user wants to escape from the console.
        /// </summary>
        public static bool Kilepett = false;

        private static void Main(string[] args)
        {
            AdatbazisEntities db = new AdatbazisEntities();
            ILogic FelsookLogic = new FelsooktLogic();

            Menu mr = new Menu();
            string[] menuPontok =
{
            "Hallgató készítése",
            "Hallgató módosítása",
            "Hallgató törlése",
            "Egy hallgató lekérése",
            "Összes hallgató lekérése",
            "Szakok létszáma",
            "Szakok átlaga",
            "Legnépszerűbb kar",
            "Java végpont lekérése",
};
            while (!Kilepett)
            {
                Menu.MenuKiirasa(menuPontok);
                if (Kilepett)
                {
                    break;
                }

                Menu.MuveletVegrehajtas(menuPontok, FelsookLogic);
            }
        }
    }
}
