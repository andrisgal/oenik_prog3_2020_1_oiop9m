﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Felsookt.Program
{
    using System;
    using System.Xml.Linq;
    using Felsookt.Data;
    using Felsookt.Logic;

    /// <summary>
    /// this is the class which writes the menu to console.
    /// </summary>
    internal class Menu
    {
        private static Random r = new Random();

        /// <summary>
        /// which menu point the cursor is on.
        /// </summary>
        private static int idx = 0;

        /// <summary>
        /// writing the menu points to console.
        /// </summary>
        /// <param name="menuPontok">name of the menu points.</param>
        public static void MenuKiirasa(string[] menuPontok)
        {
            bool vege = false;
            while (!vege)
            {
                Console.Clear();
                for (int i = 0; i < menuPontok.Length; i++)
                {
                    if (i == idx)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkYellow;
                        Console.ForegroundColor = ConsoleColor.DarkBlue;
                        Console.WriteLine(menuPontok[i]);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine(menuPontok[i]);
                    }
                }

                vege = BillentyuVizsgalat(menuPontok.Length);
            }
        }

        /// <summary>
        /// checks, which button the user pressed.
        /// </summary>
        /// <param name="tombMeret">the amount of the menu points.</param>
        /// <returns>returns the value of the pushed button.</returns>
        private static bool BillentyuVizsgalat(int tombMeret)
        {
            ConsoleKeyInfo kInfo = Console.ReadKey();
            if (kInfo.Key == ConsoleKey.UpArrow)
            {
                if (idx > 0)
                {
                    idx--;
                }
            }
            else if (kInfo.Key == ConsoleKey.DownArrow)
            {
                if (idx < tombMeret - 1)
                {
                    idx++;
                }
            }
            else if (kInfo.Key == ConsoleKey.Enter)
            {
                return true;
            }
            else if (kInfo.Key == ConsoleKey.Escape)
            {
                Program.Kilepett = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// checks whether the user pressed escape.
        /// </summary>
        private static void VegEllenorzes()
        {
            Console.WriteLine("Nyomj egy gombot a folytatáshoz...");
            ConsoleKeyInfo kInfo = Console.ReadKey();
            if (kInfo.Key == ConsoleKey.Escape)
            {
                Program.Kilepett = true;
            }
        }

        /// <summary>
        /// the selected menu point writes something on the console.
        /// </summary>
        /// <param name="menuPontok">the name of the menu points.</param>
        public static void MuveletVegrehajtas(string[] menuPontok, ILogic ilog)
        {
            AdatbazisEntities db = new AdatbazisEntities();
            FelsooktLogic ul = new FelsooktLogic();
            if (menuPontok[idx] == "Hallgató készítése")
            {
                Console.Write("ID: ");
                int v1 = int.Parse(Console.ReadLine());
                Console.Write("Szak ID: ");
                int v2 = int.Parse(Console.ReadLine());
                Console.Write("Név: ");
                string v3 = Console.ReadLine();
                Console.Write("Születési év: ");
                int v4 = int.Parse(Console.ReadLine());
                Console.Write("Átlag: ");
                int v5 = int.Parse(Console.ReadLine());
                HALLGATOK hallgato = new HALLGATOK()
                {
                    ID = v1,
                    SZAKID = v2,
                    NEV = v3.ToUpper(),
                    SZULETESIEV = v4,
                    ATLAG = v5,
                };
                ul.HallgatoKeszites(hallgato);
                Console.WriteLine("sikerült a hallgató elkészítése");

                VegEllenorzes();
            }

            if (menuPontok[idx] == "Hallgató módosítása")
            {
                Console.Write("ID: ");
                int v1 = int.Parse(Console.ReadLine());
                Console.Write("Név: ");
                string v3 = Console.ReadLine();
                ul.HallgatoModositas(v1, v3.ToUpper());
                Console.WriteLine("sikerült a módosítás");

                VegEllenorzes();
            }

            if (menuPontok[idx] == "Hallgató törlése")
            {
                Console.Write("ID: ");
                int v1 = int.Parse(Console.ReadLine());
                ul.HallgatoTorles(v1);
                Console.WriteLine("sikerült a törlés");

                VegEllenorzes();
            }

            if (menuPontok[idx] == "Egy hallgató lekérése")
            {
                Console.Write("ID: ");
                int v1 = int.Parse(Console.ReadLine());
                var kivalasztott = ul.EgyHallgato(v1);
                Console.WriteLine("------ADATAI------");
                Console.WriteLine("ID: " + kivalasztott.ID);
                Console.WriteLine("SzakID: " + kivalasztott.SZAKID);
                Console.WriteLine("Név: " + kivalasztott.NEV);
                Console.WriteLine("Születési év: " + kivalasztott.SZULETESIEV);
                Console.WriteLine("Átlag: " + kivalasztott.ATLAG);
                Console.WriteLine("-----------------");
                Console.WriteLine("sikeres a lekérdezés");
                VegEllenorzes();
            }

            if (menuPontok[idx] == "Összes hallgató lekérése")
            {
                int leghosszabbNev = 0;
                foreach (var item in ul.OsszesHallgato())
                {
                    if (leghosszabbNev < item.NEV.Length)
                    {
                        leghosszabbNev = item.NEV.Length;
                    }
                }

                foreach (var item in ul.OsszesHallgato())
                {
                    while (item.NEV.Length < leghosszabbNev)
                    {
                        item.NEV += " ";
                    }

                    Console.WriteLine(item.ID + " | " + item.SZAKID + " | " + item.NEV + " | " + item.SZULETESIEV + " | " + item.ATLAG + " | " + item.SZULETESIHELY + " | " + item.FELEVEKSZAMA);
                }

                Console.WriteLine("sikeres a lekérdezés");
                VegEllenorzes();
            }

            if (menuPontok[idx] == "Szakok létszáma")
            {
                int leghosszabbSzakNev = 0;
                foreach (var item in ul.SzakokHallgatoinakSzama())
                {
                    if (leghosszabbSzakNev < item.SzakNeve.Length)
                    {
                        leghosszabbSzakNev = item.SzakNeve.Length;
                    }
                }

                foreach (var item in ul.SzakokHallgatoinakSzama())
                {
                    while (item.SzakNeve.Length < leghosszabbSzakNev)
                    {
                        item.SzakNeve += " ";
                    }

                    Console.WriteLine(item.SzakNeve + " | | " + item.HallgatokSzama);
                }

                Console.WriteLine("sikeres a lekérdezés");
                VegEllenorzes();
            }

            if (menuPontok[idx] == "Szakok átlaga")
            {
                int leghosszabbSzakNev = 0;
                foreach (var item in ul.SzakokAtlaga())
                {
                    if (leghosszabbSzakNev < item.SzakNeve.Length)
                    {
                        leghosszabbSzakNev = item.SzakNeve.Length;
                    }
                }

                foreach (var item in ul.SzakokAtlaga())
                {
                    while (item.SzakNeve.Length < leghosszabbSzakNev)
                    {
                        item.SzakNeve += " ";
                    }

                    Console.WriteLine(item.SzakNeve + " | | " + item.Atlagok);
                }

                Console.WriteLine("sikeres a lekérdezés");
                VegEllenorzes();
            }

            if (menuPontok[idx] == "Legnépszerűbb kar")
            {
                Console.WriteLine(ul.LegnepszerubbKar());
                VegEllenorzes();
            }

            if (menuPontok[idx] == "Java végpont lekérése")
            {
                Console.Write("Adjon meg egy nevet: ");
                string v1 = Console.ReadLine().ToUpper();
                Console.Write("Adjon meg egy születési  évet: ");
                int v2 = int.Parse(Console.ReadLine());
                Console.Write("Adjon meg egy átlagot: ");
                int v3 = int.Parse(Console.ReadLine());
                string url = $"http://localhost:8080/OENIK_PROG3_2020_1_OIOP9M/Kiiras";
                XDocument xdoc = XDocument.Load(url);
                xdoc.Save("kimenet.txt");
                Console.WriteLine("sikeres a lekérdezés");
                string[] lines = System.IO.File.ReadAllLines(@"C:\Users\User\Desktop\OIOP9M\CSHARP\OENIK_PROG3_2020_1_OIOP9M\Felsookt.Program\bin\Debug\kimenet.txt");

                foreach (string line in lines)
                {
                    // Use a tab to indent each line of the file.
                    Console.WriteLine("\t" + line);
                }

                VegEllenorzes();
            }
        }
    }
}
