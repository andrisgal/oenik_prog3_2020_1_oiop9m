﻿// <copyright file="IRepository{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Felsookt.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using Felsookt.Data;

    /// <summary>
    /// the interface where we create the read methods.
    /// </summary>
    /// <typeparam name="T">it will be a "HALLGATOK" type.</typeparam>
    public interface IRepository
    {
        /// <summary>
        /// read method 1.
        /// </summary>
        /// <param name="id">Students ID.</param>
        /// <returns>returns the Student which has this id.</returns>
        HALLGATOK EgyHallgato(int id);

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">Kar ID.</param>
        /// <returns>returns the Kar which has this id.</returns>
        KAROK EgyKar(int id);

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">Szak ID.</param>
        /// <returns>returns the Kar which has this id.</returns>
        SZAKOK Egyszak(int id);

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>returns all of the Kar.</returns>
        List<KAROK> OsszesKar();

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>returns all of the Szak.</returns>
        List<SZAKOK> OsszesSzak();

        /// <summary>
        /// read method 2.
        /// </summary>
        /// <returns>returns all of the Students.</returns>
        List<HALLGATOK> OsszesHallgato();
    }

    /// <summary>
    /// interface which create the remaining crud methods.
    /// </summary>
    public interface IFelsooktRepository : IRepository
    {
        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="hallgato">create Student.</param>
        void HallgatoKeszites(HALLGATOK hallgato);

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="kar">create Kar.</param>
        void KarKeszites(KAROK kar);

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="szak">create Szak.</param>
        void SzakKeszites(SZAKOK szak);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which Student we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        void HallgatoModositas(int id, string ujNev);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which Szak we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        void SzakModositas(int id, string ujNev);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which Kar we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        void KarModositas(int id, string ujNev);

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        void HallgatoTorles(int id);

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        void SzaktoTorles(int id);

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        void KarTorles(int id);
    }

    /// <summary>
    /// the class where we implement all of the crud methods and non-crud methods.
    /// </summary>
    public class FelsooktRepository : IFelsooktRepository
    {
        private AdatbazisEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="FelsooktRepository"/> class.
        /// </summary>
        /// <param name="db">the database.</param>
        public FelsooktRepository(AdatbazisEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// read method 1.
        /// </summary>
        /// <param name="id">the wanted Student.</param>
        /// <returns>the Student with the same ID.</returns>
        public HALLGATOK EgyHallgato(int id)
        {
            return db.HALLGATOK.Where(x => x.ID == id).FirstOrDefault();
        }

        /// <summary>
        /// read method 2.
        /// </summary>
        /// <returns>all of the Student.</returns>
        public List<HALLGATOK> OsszesHallgato()
        {
            return this.db.HALLGATOK.ToList();
        }

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="hallgato">the created Student.</param>
        public void HallgatoKeszites(HALLGATOK hallgato)
        {
            this.db.HALLGATOK.Add(hallgato);
            this.db.SaveChanges();
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        public void HallgatoModositas(int id, string ujNev)
        {
            var modositando = this.EgyHallgato(id);
            modositando.NEV = ujNev;
            this.db.SaveChanges();
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        public void HallgatoTorles(int id)
        {
            this.db.HALLGATOK.Remove(this.EgyHallgato(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="kar">the created Kar.</param>
        public void KarKeszites(KAROK kar)
        {
            this.db.KAROK.Add(kar);
            this.db.SaveChanges();
        }

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="szak">the created Szak.</param>
        public void SzakKeszites(SZAKOK szak)
        {
            this.db.SZAKOK.Add(szak);
            this.db.SaveChanges();
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">the wanted Kar.</param>
        /// <returns>the Kar with the same ID.</returns>
        public KAROK EgyKar(int id)
        {
            return this.db.KAROK.Where(x => x.ID == id).FirstOrDefault();
        }

        /// <summary>
        /// read method 1.
        /// </summary>
        /// <param name="id">the wanted Szak.</param>
        /// <returns>the Szak with the same ID.</returns>
        public SZAKOK Egyszak(int id)
        {
            return this.db.SZAKOK.Where(x => x.ID == id).FirstOrDefault();
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>all of the Kar.</returns>
        public List<KAROK> OsszesKar()
        {
            return this.db.KAROK.ToList();
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>all of the Szak.</returns>
        public List<SZAKOK> OsszesSzak()
        {
            return this.db.SZAKOK.ToList();
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        public void SzakModositas(int id, string ujNev)
        {
            var modositando = this.Egyszak(id);
            modositando.NEV = ujNev;
            this.db.SaveChanges();
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        public void KarModositas(int id, string ujNev)
        {
            var modositando = this.EgyKar(id);
            modositando.NEV = ujNev;
            this.db.SaveChanges();
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        public void SzaktoTorles(int id)
        {
            this.db.SZAKOK.Remove(this.Egyszak(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        public void KarTorles(int id)
        {
            this.db.KAROK.Remove(this.EgyKar(id));
            this.db.SaveChanges();
        }
    }
}