﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Felsookt.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Felsookt.Data;
    using Felsookt.Logic;
    using Felsookt.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// moq tests' class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        private Mock<IFelsooktRepository> mockRepo;
        private FelsooktLogic uniLog;

        /// <summary>
        /// initiating the mockrepo and unilog.
        /// </summary>
        [SetUp]
        public void Init()
        {
            List<HALLGATOK> testHallgatoData = new List<HALLGATOK>()
            {
                new HALLGATOK() { ID = 0, SZAKID = 0, NEV = "MARTIN", SZULETESIEV = 1990, ATLAG = 320, FELEVEKSZAMA = 2, SZULETESIHELY = "Budapest" },
                new HALLGATOK() { ID = 1, SZAKID = 1, NEV = "ALLEN", SZULETESIEV = 1988, ATLAG = 243, FELEVEKSZAMA = 3, SZULETESIHELY = "Budapest" },
                new HALLGATOK() { ID = 2, SZAKID = 2, NEV = "TURNER", SZULETESIEV = 1977, ATLAG = 211, FELEVEKSZAMA = 2, SZULETESIHELY = "Budapest" },
                new HALLGATOK() { ID = 3, SZAKID = 0, NEV = "JAMES", SZULETESIEV = 1980, ATLAG = 220, FELEVEKSZAMA = 1, SZULETESIHELY = "Budapest" },
                new HALLGATOK() { ID = 4, SZAKID = 0, NEV = "WARD", SZULETESIEV = 1991, ATLAG = 187, FELEVEKSZAMA = 5, SZULETESIHELY = "Budapest" },
            };

            List<KAROK> testKarData = new List<KAROK>()
            {
                new KAROK() { ID = 0, NEV = "KGK", HELY = "Budapest", TIPUS = "Gazdasagi", ALAPITASEVE = 1980, BEVETEL = 200000, HALLGATOKSZAMA = 45 },
                new KAROK() { ID = 1, NEV = "NIK", HELY = "OBUDA", TIPUS = "Informatika", ALAPITASEVE = 1980, BEVETEL = 200000, HALLGATOKSZAMA = 45 },
                new KAROK() { ID = 2, NEV = "Banki", HELY = "Budapest", TIPUS = "Gepesz", ALAPITASEVE = 1980, BEVETEL = 200000, HALLGATOKSZAMA = 45 },
                new KAROK() { ID = 3, NEV = "KGK", HELY = "Budapest", TIPUS = "Gazdasagi", ALAPITASEVE = 1980, BEVETEL = 200000, HALLGATOKSZAMA = 45 },
            };

            List<SZAKOK> testSzakData = new List<SZAKOK>()
            {
                new SZAKOK() { ID = 0, KARID = 0, NEV = "Gazdasaginformatikus", EPULET = "TG", HALLGATOK = 65, SZAKATLAG = 4, ALAPITASEVE = 1989 },
                new SZAKOK() { ID = 1, KARID = 2, NEV = "Gepeszmernok", EPULET = "N", HALLGATOK = 78, SZAKATLAG = 2,  ALAPITASEVE = 1989 },
                new SZAKOK() { ID = 2, KARID = 1, NEV = "Mernokinformatikus", EPULET = "BA", HALLGATOK = 78, SZAKATLAG = 2, ALAPITASEVE = 1989 },
            };

            this.mockRepo = new Mock<IFelsooktRepository>();

            this.mockRepo.Setup(x => x.OsszesHallgato()).Returns(testHallgatoData);

            this.mockRepo.Setup(x => x.OsszesKar()).Returns(testKarData);

            this.mockRepo.Setup(x => x.OsszesSzak()).Returns(testSzakData);

            this.mockRepo.Setup(x => x.EgyHallgato(It.IsAny<int>()))
           .Returns((int i) => testHallgatoData.Where(y => y.ID.Equals(i)).SingleOrDefault() ?? new HALLGATOK());

            this.mockRepo.Setup(x => x.Egyszak(It.IsAny<int>()))
                .Returns((int i) => testSzakData.Where(y => y.ID.Equals(i)).SingleOrDefault() ?? new SZAKOK());

            this.mockRepo.Setup(x => x.EgyKar(It.IsAny<int>()))
                .Returns((int i) => testKarData.Where(y => y.ID.Equals(i)).SingleOrDefault() ?? new KAROK());

            this.uniLog = new FelsooktLogic(this.mockRepo.Object);
        }

        /// <summary>
        /// moq create method.
        /// </summary>
        [Test]
        public void HallgatoKeszites_MoqTest()
        {
            this.uniLog.HallgatoKeszites(new HALLGATOK
            {
                ID = 45,
                SZAKID = 6,
                NEV = "JORDAN",
                SZULETESIEV = 1998,
                ATLAG = 445,
            });
            this.mockRepo.Verify(x => x.HallgatoKeszites(It.IsAny<HALLGATOK>()), Times.Once);
        }

        /// <summary>
        /// moq create method.
        /// </summary>
        [Test]
        public void KarKeszites_MoqTest()
        {
            this.uniLog.KarKeszites(new KAROK
            {
                ID = 45,
                NEV = "KGK",
                HELY = "Budapest",
                TIPUS = "Gazdasagi",
            });
            this.mockRepo.Verify(x => x.KarKeszites(It.IsAny<KAROK>()), Times.Once);
        }

        /// <summary>
        /// moq create method.
        /// </summary>
        [Test]
        public void SzakKeszites_MoqTest()
        {
            this.uniLog.SzakKeszites(new SZAKOK
            {
                ID = 0,
                KARID = 0,
                NEV = "Naplopo",
                EPULET = "TG",
                HALLGATOK = 67,
                SZAKATLAG = 4,
            });
            this.mockRepo.Verify(x => x.SzakKeszites(It.IsAny<SZAKOK>()), Times.Once);
        }

        /// <summary>
        /// moq update method.
        /// </summary>
        [Test]
        public void HallgatoModositas_MoqTest()
        {
            int id = 13;
            string ujNev = "BENŐ";
            this.uniLog.HallgatoModositas(id, ujNev);
            this.mockRepo.Verify(x => x.HallgatoModositas(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// moq update method.
        /// </summary>
        [Test]
        public void SzakModositas_MoqTest()
        {
            int id = 14;
            string ujNev = "Mérnök";
            this.uniLog.SzakModositas(id, ujNev);
            this.mockRepo.Verify(x => x.SzakModositas(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// moq delete method.
        /// </summary>
        [Test]
        public void HallgatoTorles_MoqTest()
        {
            int id = 11;
            this.uniLog.HallgatoTorles(id);
            this.mockRepo.Verify(x => x.HallgatoTorles(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// moq delete method.
        /// </summary>
        [Test]
        public void SzakTorles_MoqTest()
        {
            int id = 14;
            this.uniLog.SzaktoTorles(id);
            this.mockRepo.Verify(x => x.SzaktoTorles(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// moq delete method.
        /// </summary>
        [Test]
        public void KarTorles_MoqTest()
        {
            int id = 11;
            this.uniLog.KarTorles(id);
            this.mockRepo.Verify(x => x.KarTorles(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// moq read method 1.
        /// </summary>
        [Test]
        public void EgyHallgato_MoqTest()
        {
            HALLGATOK kiv = this.uniLog.EgyHallgato(2);
            Assert.AreEqual(kiv.NEV, "TURNER");
            this.mockRepo.Verify(x => x.EgyHallgato(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// moq read method.
        /// </summary>
        [Test]
        public void EgySzak_MoqTest()
        {
            SZAKOK kiv = this.uniLog.Egyszak(2);
            Assert.AreEqual(kiv.NEV, "Mernokinformatikus");
            this.mockRepo.Verify(x => x.Egyszak(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// moq read method.
        /// </summary>
        [Test]
        public void EgyKar_MoqTest()
        {
            KAROK kiv = this.uniLog.EgyKar(2);
            Assert.AreEqual(kiv.NEV, "Banki");
            this.mockRepo.Verify(x => x.EgyKar(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// moq read method.
        /// </summary>
        [Test]
        public void OsszesHallgato_MoqTest()
        {
            List<HALLGATOK> hallgatok = this.uniLog.OsszesHallgato();
            this.mockRepo.Verify(x => x.OsszesHallgato(), Times.Once);
        }

        /// <summary>
        /// moq read method.
        /// </summary>
        [Test]
        public void OsszesKar_MoqTest()
        {
            List<KAROK> karok = this.uniLog.OsszesKar();
            this.mockRepo.Verify(x => x.OsszesKar(), Times.Once);
        }

        /// <summary>
        /// moq read method.
        /// </summary>
        [Test]
        public void OsszesSzak_MoqTest()
        {
            List<SZAKOK> szakok = this.uniLog.OsszesSzak();
            this.mockRepo.Verify(x => x.OsszesSzak(), Times.Once);
        }

        /// <summary>
        /// moq departments' Students' number.
        /// </summary>
        [Test]
        public void SzakokHallgatoinakSzama_MoqTest()
        {
            IEnumerable<SzakokNepessege> result = this.uniLog.SzakokHallgatoinakSzama();
            Assert.AreEqual(result.ElementAt(0).HallgatokSzama, 3);
            Assert.AreEqual(result.ElementAt(1).HallgatokSzama, 1);
        }

        /// <summary>
        /// moq departments' average.
        /// </summary>
        [Test]
        public void SzakokAtlaga_MoqTest()
        {
            IEnumerable<Atlag> result = this.uniLog.SzakokAtlaga();
            Assert.AreEqual(result.ElementAt(0).Atlagok, 242.00);
        }

        /// <summary>
        /// moq the most liked Faculty.
        /// </summary>
        [Test]
        public void LegnepszerubbKar_MoqTest()
        {
            string result = this.uniLog.LegnepszerubbKar();
            Assert.AreEqual("KGK", result);
        }
    }
}