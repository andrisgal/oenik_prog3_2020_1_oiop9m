﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Felsookt.Data;

namespace Felsookt.Logic
{
    /// <summary>
    /// was made to be able to write to console the departments name and averages.
    /// </summary>
    public class Atlag
    {
        /// <summary>
        /// Gets or sets faculty's name.
        /// </summary>
        public string SzakNeve { get; set; }

        /// <summary>
        /// Gets or sets faculty's average.
        /// </summary>
        public double Atlagok { get; set; }
    }
}