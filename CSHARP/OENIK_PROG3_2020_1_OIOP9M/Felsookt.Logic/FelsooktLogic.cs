﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Felsookt.Data;
using Felsookt.Repository;
namespace Felsookt.Logic
{
    /// <summary>
    /// the class where the methods are handled.
    /// </summary>
    public class FelsooktLogic : ILogic
    {
        private IFelsooktRepository uniRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FelsooktLogic"/> class.
        /// </summary>
        public FelsooktLogic()
        {
            this.uniRepo = new FelsooktRepository(new AdatbazisEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FelsooktLogic"/> class.
        /// </summary>
        /// <param name="uniRepo">an instance from IFelsooktRepository.</param>
        public FelsooktLogic(IFelsooktRepository uniRepo)
        {
            this.uniRepo = uniRepo;
        }

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="hallgato">created Student.</param>
        public void HallgatoKeszites(HALLGATOK hallgato)
        {
            this.uniRepo.HallgatoKeszites(hallgato);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        public void HallgatoModositas(int id, string ujNev)
        {
            this.uniRepo.HallgatoModositas(id, ujNev);
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        public void HallgatoTorles(int id)
        {
            this.uniRepo.HallgatoTorles(id);
        }

        /// <summary>
        /// read method 1.
        /// </summary>
        /// <param name="id">the wanted Student.</param>
        /// <returns>the Student which has the same ID.</returns>
        public HALLGATOK EgyHallgato(int id)
        {
            return this.uniRepo.EgyHallgato(id);
        }

        /// <summary>
        /// read method 2.
        /// </summary>
        /// <returns>all of the Students.</returns>
        public List<HALLGATOK> OsszesHallgato()
        {
            return this.uniRepo.OsszesHallgato().ToList();
        }

        /// <summary>
        /// Department's counted Students.
        /// </summary>
        /// <returns>how many Students each Departments have.</returns>
        public List<SzakokNepessege> SzakokHallgatoinakSzama()
        {
            var q = from h in this.uniRepo.OsszesHallgato()
                    from k in this.uniRepo.OsszesKar()
                    from sz in this.uniRepo.OsszesSzak()
                    where h.SZAKID == sz.ID && k.ID == sz.KARID
                    group h by sz.NEV into g
                    orderby g.Count() descending
                    select new SzakokNepessege
                    {
                        SzakNeve = g.Key,
                        HallgatokSzama = g.Count(),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Departments' average.
        /// </summary>
        /// <returns>Departments' students' average.</returns>
        public List<Atlag> SzakokAtlaga()
        {
            var atlag = from h in this.uniRepo.OsszesHallgato()
                        from k in this.uniRepo.OsszesKar()
                        from sz in this.uniRepo.OsszesSzak()
                        where h.SZAKID == sz.ID && k.ID == sz.KARID
                        group h by sz.NEV into g
                        select new Atlag
                        {
                            SzakNeve = g.Key,
                            Atlagok = Math.Round((double)g.Average(x => x.ATLAG)),
                        };

            return atlag.ToList();
        }

        /// <summary>
        /// The most liked Faculty.
        /// </summary>
        /// <returns>the name of the most liked Faculty.</returns>
        public string LegnepszerubbKar()
        {
            var legnepszerubb = from h in this.uniRepo.OsszesHallgato()
                                from k in this.uniRepo.OsszesKar()
                                from sz in this.uniRepo.OsszesSzak()
                                where h.SZAKID == sz.ID && k.ID == sz.KARID
                                group h by k.NEV into queryGroup
                                orderby queryGroup.Count() descending
                                select queryGroup.Key;

            return legnepszerubb.FirstOrDefault();
        }

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="kar">created Kar.</param>
        public void KarKeszites(KAROK kar)
        {
            this.uniRepo.KarKeszites(kar);
        }

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="szak">created Szak.</param>
        public void SzakKeszites(SZAKOK szak)
        {
            this.uniRepo.SzakKeszites(szak);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        public void SzakModositas(int id, string ujNev)
        {
            this.uniRepo.SzakModositas(id, ujNev);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the name given to it.</param>
        public void KarModositas(int id, string ujNev)
        {
            this.uniRepo.SzakModositas(id, ujNev);
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        public void SzaktoTorles(int id)
        {
            this.uniRepo.SzaktoTorles(id);
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        public void KarTorles(int id)
        {
            this.uniRepo.KarTorles(id);
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">the wanted Kar.</param>
        /// <returns>the Kar which has the same ID.</returns>
        public KAROK EgyKar(int id)
        {
           return this.uniRepo.EgyKar(id);
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">the wanted Szak.</param>
        /// <returns>the Szak which has the same ID.</returns>
        public SZAKOK Egyszak(int id)
        {
           return this.uniRepo.Egyszak(id);
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>all of the Kar.</returns>
        public List<KAROK> OsszesKar()
        {
           return this.uniRepo.OsszesKar();
        }

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>all of Szak.</returns>
        public List<SZAKOK> OsszesSzak()
        {
          return this.uniRepo.OsszesSzak();
        }
    }
}
