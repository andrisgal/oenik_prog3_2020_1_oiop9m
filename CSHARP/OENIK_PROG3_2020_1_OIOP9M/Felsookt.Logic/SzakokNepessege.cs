﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Felsookt.Logic
{
    public class SzakokNepessege
    {
        /// <summary>
        /// Gets or sets Faculty's name.
        /// </summary>
        public string SzakNeve { get; set; }

        /// <summary>
        /// Gets or sets how many Students the departments have.
        /// </summary>
        public int HallgatokSzama { get; set; }
    }
}
