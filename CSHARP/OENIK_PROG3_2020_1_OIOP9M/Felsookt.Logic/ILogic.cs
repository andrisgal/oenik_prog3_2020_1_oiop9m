﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Felsookt.Data;

namespace Felsookt.Logic
{
    /// <summary>
    /// contains the 3 crud methods and 5 non-crud methods.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="hallgato">created Student.</param>
        void HallgatoKeszites(HALLGATOK hallgato);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the new name given to it.</param>
        void HallgatoModositas(int id, string ujNev);

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">which we want to delete.</param>
        void HallgatoTorles(int id);
  

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">the searched id.</param>
        /// <returns>the Student which has the given id.</returns>
        HALLGATOK EgyHallgato(int id);

        /// <summary>
        /// read method 2.
        /// </summary>
        /// <returns>all the Students.</returns>
        List<HALLGATOK> OsszesHallgato();

        /// <summary>
        ///  returns how many Students are in each departments.
        /// </summary>
        /// <returns>how many Students are in each departments.</returns>
        List<SzakokNepessege> SzakokHallgatoinakSzama();

        /// <summary>
        /// Departments' average.
        /// </summary>
        /// <returns>the Departments' average.</returns>
        List<Atlag> SzakokAtlaga();

        /// <summary>
        /// The most liked Faculty.
        /// </summary>
        /// <returns>which faculty has the most Students.</returns>
        string LegnepszerubbKar();

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="kar">created Kar.</param>
        void KarKeszites(KAROK kar);

        /// <summary>
        /// create method.
        /// </summary>
        /// <param name="szak">created Szak.</param>
        void SzakKeszites(SZAKOK szak);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the new name given to it.</param>
        void SzakModositas(int id, string ujNev);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the new name given to it.</param>
        void KarModositas(int id, string ujNev);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the new name given to it.</param>
        void SzaktoTorles(int id);

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">which we want to update.</param>
        /// <param name="ujNev">the new name given to it.</param>
        void KarTorles(int id);

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">the searched id.</param>
        /// <returns>the Kar which has the given id.</returns>
        KAROK EgyKar(int id);

        /// <summary>
        /// read method.
        /// </summary>
        /// <param name="id">the searched id.</param>
        /// <returns>the Szak which has the given id.</returns>
        SZAKOK Egyszak(int id);

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>all the Kar.</returns>
        List<KAROK> OsszesKar();

        /// <summary>
        /// read method.
        /// </summary>
        /// <returns>all the Szak.</returns>
        List<SZAKOK> OsszesSzak();
    }
}
