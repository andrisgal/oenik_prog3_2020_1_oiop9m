var searchData=
[
  ['data_7',['Data',['../namespace_felsookt_1_1_data.html',1,'Felsookt']]],
  ['felsookt_8',['Felsookt',['../namespace_felsookt.html',1,'']]],
  ['felsooktlogic_9',['FelsooktLogic',['../class_felsookt_1_1_logic_1_1_felsookt_logic.html',1,'Felsookt.Logic.FelsooktLogic'],['../class_felsookt_1_1_logic_1_1_felsookt_logic.html#a55a3969c4b1935387075c5c29be8a5df',1,'Felsookt.Logic.FelsooktLogic.FelsooktLogic()'],['../class_felsookt_1_1_logic_1_1_felsookt_logic.html#a4a9b2325e0feed39d2aa8f210d6a0afc',1,'Felsookt.Logic.FelsooktLogic.FelsooktLogic(IFelsooktRepository uniRepo)']]],
  ['felsooktrepository_10',['FelsooktRepository',['../class_felsookt_1_1_repository_1_1_felsookt_repository.html',1,'Felsookt.Repository.FelsooktRepository'],['../class_felsookt_1_1_repository_1_1_felsookt_repository.html#a512ebb999e040deb5885fbf5c893fc36',1,'Felsookt.Repository.FelsooktRepository.FelsooktRepository()']]],
  ['logic_11',['Logic',['../namespace_felsookt_1_1_logic.html',1,'Felsookt']]],
  ['program_12',['Program',['../namespace_felsookt_1_1_program.html',1,'Felsookt']]],
  ['repository_13',['Repository',['../namespace_felsookt_1_1_repository.html',1,'Felsookt']]],
  ['tests_14',['Tests',['../namespace_felsookt_1_1_logic_1_1_tests.html',1,'Felsookt::Logic']]]
];
