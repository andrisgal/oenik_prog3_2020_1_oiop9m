var searchData=
[
  ['szakkeszites_37',['SzakKeszites',['../class_felsookt_1_1_repository_1_1_felsookt_repository.html#ae7cf732a99e2a5ac85eef0eb3afba7b6',1,'Felsookt::Repository::FelsooktRepository']]],
  ['szakkeszites_5fmoqtest_38',['SzakKeszites_MoqTest',['../class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#acffa4e0f701bde07c218da048903ebf0',1,'Felsookt::Logic::Tests::Tests']]],
  ['szakmodositas_5fmoqtest_39',['SzakModositas_MoqTest',['../class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ab8403752a3fe11688e0cfa011762f232',1,'Felsookt::Logic::Tests::Tests']]],
  ['szakneve_40',['SzakNeve',['../class_felsookt_1_1_logic_1_1_atlag.html#a9a6a80793a04ef68e4a22677928c43dd',1,'Felsookt.Logic.Atlag.SzakNeve()'],['../class_felsookt_1_1_logic_1_1_szakok_nepessege.html#ae3889f71cf3028dc7d3acf5edd839273',1,'Felsookt.Logic.SzakokNepessege.SzakNeve()']]],
  ['szakok_41',['SZAKOK',['../class_felsookt_1_1_data_1_1_s_z_a_k_o_k.html',1,'Felsookt::Data']]],
  ['szakokatlaga_42',['SzakokAtlaga',['../class_felsookt_1_1_logic_1_1_felsookt_logic.html#aeec069915c579872362aa5da88447363',1,'Felsookt.Logic.FelsooktLogic.SzakokAtlaga()'],['../interface_felsookt_1_1_logic_1_1_i_logic.html#a5adb5ce1f3a08fe7b83d972ebfabb38d',1,'Felsookt.Logic.ILogic.SzakokAtlaga()']]],
  ['szakokatlaga_5fmoqtest_43',['SzakokAtlaga_MoqTest',['../class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a74403dc0df310b6b84d521dbd34338d2',1,'Felsookt::Logic::Tests::Tests']]],
  ['szakokhallgatoinakszama_44',['SzakokHallgatoinakSzama',['../class_felsookt_1_1_logic_1_1_felsookt_logic.html#a5955349c38f1f91c5363d978c1f53f4e',1,'Felsookt.Logic.FelsooktLogic.SzakokHallgatoinakSzama()'],['../interface_felsookt_1_1_logic_1_1_i_logic.html#abe9bf9cb78a1dfd4c940ff76f78ac088',1,'Felsookt.Logic.ILogic.SzakokHallgatoinakSzama()']]],
  ['szakokhallgatoinakszama_5fmoqtest_45',['SzakokHallgatoinakSzama_MoqTest',['../class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a9a1506b73e5b464d20e21a9491e94d3e',1,'Felsookt::Logic::Tests::Tests']]],
  ['szakoknepessege_46',['SzakokNepessege',['../class_felsookt_1_1_logic_1_1_szakok_nepessege.html',1,'Felsookt::Logic']]],
  ['szaktorles_5fmoqtest_47',['SzakTorles_MoqTest',['../class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ac47b4141d5bccfd280ad2abf126472ff',1,'Felsookt::Logic::Tests::Tests']]]
];
