var class_felsookt_1_1_logic_1_1_felsookt_logic =
[
    [ "FelsooktLogic", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a55a3969c4b1935387075c5c29be8a5df", null ],
    [ "FelsooktLogic", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a4a9b2325e0feed39d2aa8f210d6a0afc", null ],
    [ "EgyHallgato", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#aafaf957ab979eca862462c59bf960ff3", null ],
    [ "EgyKar", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a43b8419e3212ceacf6b35c200a8fc50f", null ],
    [ "Egyszak", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#aae682b0d4b8520590bbf644f50e151e0", null ],
    [ "HallgatoKeszites", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a076fccd128a91b73774ba7170df32094", null ],
    [ "HallgatoModositas", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a4ddea2ab3e33bde0ed0c9f59806c0ac4", null ],
    [ "HallgatoTorles", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a3164fc958047421fc22a7ba110a0bb18", null ],
    [ "KarKeszites", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a7f6e28aae26bc9b0054fc108b6b93188", null ],
    [ "KarModositas", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a2d3e1487c7df63ad80d3198273aba517", null ],
    [ "KarTorles", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a446e22f0077428275037b4b5e1d31afb", null ],
    [ "LegnepszerubbKar", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a1b8f0b652338a51bcb325470f0b770f9", null ],
    [ "OsszesHallgato", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a964e7c0a6d857f6120ad3c955efe991f", null ],
    [ "OsszesKar", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#af78dece1374b10c71e4cdea08b25ec83", null ],
    [ "OsszesSzak", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#ac79292067c6d5ef5a83bb1a29e78787d", null ],
    [ "SzakKeszites", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#af5e77f2efffef4419598f9eac8fbe693", null ],
    [ "SzakModositas", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a29b6bbc9a27cbc37427ea7a243c09895", null ],
    [ "SzakokAtlaga", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#aeec069915c579872362aa5da88447363", null ],
    [ "SzakokHallgatoinakSzama", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#a5955349c38f1f91c5363d978c1f53f4e", null ],
    [ "SzaktoTorles", "class_felsookt_1_1_logic_1_1_felsookt_logic.html#ad06eca22e031116fe11c86cf8fca5b6a", null ]
];