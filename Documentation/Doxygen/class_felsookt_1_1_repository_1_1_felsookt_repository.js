var class_felsookt_1_1_repository_1_1_felsookt_repository =
[
    [ "FelsooktRepository", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a512ebb999e040deb5885fbf5c893fc36", null ],
    [ "EgyHallgato", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#aef1c369bcf1b4a48e54f0506802f5a29", null ],
    [ "EgyKar", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#af674f4cd23cb1a88607d740f73acffe8", null ],
    [ "Egyszak", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#af355e645a5e06c02faa0a0e593a66bd3", null ],
    [ "HallgatoKeszites", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a352f66cc509d6884ab37130373bc13a5", null ],
    [ "HallgatoModositas", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a301c4ce9048cf3cfa2e47d178ad75e61", null ],
    [ "HallgatoTorles", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a3314d0a66cffcb722a60313f821246c3", null ],
    [ "KarKeszites", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#afd9a4da02fcf7337c7e043ad6442716d", null ],
    [ "KarModositas", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#af0b0503ba38299ba22e6038611a4ff0a", null ],
    [ "KarTorles", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#ab0fbca0e5ff0427cdf5d32110fd5685a", null ],
    [ "OsszesHallgato", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a5df34b2b07705c0c73803deee7b1cf8b", null ],
    [ "OsszesKar", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a6bea4070b4977c65e3ef7c9bdc89c83d", null ],
    [ "OsszesSzak", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a2a3d8c80c910e4b9e06e4b310c0a476e", null ],
    [ "SzakKeszites", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#ae7cf732a99e2a5ac85eef0eb3afba7b6", null ],
    [ "SzakModositas", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a7240b6c0eacb1f98b4a2267d2b08d895", null ],
    [ "SzaktoTorles", "class_felsookt_1_1_repository_1_1_felsookt_repository.html#a66c67d7862344ee0df9ea79b14dd01c4", null ]
];