var hierarchy =
[
    [ "Felsookt.Logic.Atlag", "class_felsookt_1_1_logic_1_1_atlag.html", null ],
    [ "DbContext", null, [
      [ "Felsookt.Data.AdatbazisEntities", "class_felsookt_1_1_data_1_1_adatbazis_entities.html", null ]
    ] ],
    [ "Felsookt.Data.HALLGATOK", "class_felsookt_1_1_data_1_1_h_a_l_l_g_a_t_o_k.html", null ],
    [ "Felsookt.Logic.ILogic", "interface_felsookt_1_1_logic_1_1_i_logic.html", [
      [ "Felsookt.Logic.FelsooktLogic", "class_felsookt_1_1_logic_1_1_felsookt_logic.html", null ]
    ] ],
    [ "Felsookt.Repository.IRepository", "interface_felsookt_1_1_repository_1_1_i_repository.html", [
      [ "Felsookt.Repository.IFelsooktRepository", "interface_felsookt_1_1_repository_1_1_i_felsookt_repository.html", [
        [ "Felsookt.Repository.FelsooktRepository", "class_felsookt_1_1_repository_1_1_felsookt_repository.html", null ]
      ] ]
    ] ],
    [ "Felsookt.Data.KAROK", "class_felsookt_1_1_data_1_1_k_a_r_o_k.html", null ],
    [ "Felsookt.Data.SZAKOK", "class_felsookt_1_1_data_1_1_s_z_a_k_o_k.html", null ],
    [ "Felsookt.Logic.SzakokNepessege", "class_felsookt_1_1_logic_1_1_szakok_nepessege.html", null ],
    [ "Felsookt.Logic.Tests.Tests", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html", null ]
];