var interface_felsookt_1_1_logic_1_1_i_logic =
[
    [ "EgyHallgato", "interface_felsookt_1_1_logic_1_1_i_logic.html#af0ba53344321eb956263f569f308198b", null ],
    [ "EgyKar", "interface_felsookt_1_1_logic_1_1_i_logic.html#a6dbf1f64901922538eb95524f39f74ff", null ],
    [ "Egyszak", "interface_felsookt_1_1_logic_1_1_i_logic.html#a58eb6e7dc059c0dd1fe1480e9746f06b", null ],
    [ "HallgatoKeszites", "interface_felsookt_1_1_logic_1_1_i_logic.html#a28943a2e823ba422e354f6a6ea652618", null ],
    [ "HallgatoModositas", "interface_felsookt_1_1_logic_1_1_i_logic.html#aabc2d409a8688d2a3d12f865ad41d7bb", null ],
    [ "HallgatoTorles", "interface_felsookt_1_1_logic_1_1_i_logic.html#a7bbe60fc28272af8104ec691d30a3200", null ],
    [ "KarKeszites", "interface_felsookt_1_1_logic_1_1_i_logic.html#a96c61230dc1bcd56bd0f65b417628c49", null ],
    [ "KarModositas", "interface_felsookt_1_1_logic_1_1_i_logic.html#abe422ea559cae7bb84414a1fbe901661", null ],
    [ "KarTorles", "interface_felsookt_1_1_logic_1_1_i_logic.html#a8042ddc8ece361e8ddecade16d40cf0e", null ],
    [ "LegnepszerubbKar", "interface_felsookt_1_1_logic_1_1_i_logic.html#a72fe72f5c54e17604fc748414b43f204", null ],
    [ "OsszesHallgato", "interface_felsookt_1_1_logic_1_1_i_logic.html#a7c543f04255fe19a3364c7e2bde4078d", null ],
    [ "OsszesKar", "interface_felsookt_1_1_logic_1_1_i_logic.html#a9fb5b3e278a26647dd9de96818f8bf76", null ],
    [ "OsszesSzak", "interface_felsookt_1_1_logic_1_1_i_logic.html#a08b145ba91411965a6b139ea2872fc52", null ],
    [ "SzakKeszites", "interface_felsookt_1_1_logic_1_1_i_logic.html#a394265655a0d57e936ab1334b64f1bdd", null ],
    [ "SzakModositas", "interface_felsookt_1_1_logic_1_1_i_logic.html#a050d886183fabbffeae1cde7953e9597", null ],
    [ "SzakokAtlaga", "interface_felsookt_1_1_logic_1_1_i_logic.html#a5adb5ce1f3a08fe7b83d972ebfabb38d", null ],
    [ "SzakokHallgatoinakSzama", "interface_felsookt_1_1_logic_1_1_i_logic.html#abe9bf9cb78a1dfd4c940ff76f78ac088", null ],
    [ "SzaktoTorles", "interface_felsookt_1_1_logic_1_1_i_logic.html#a207ab8da9b3d1c0ac76506a5a6316f4c", null ]
];