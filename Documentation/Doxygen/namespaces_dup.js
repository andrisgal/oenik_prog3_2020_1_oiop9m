var namespaces_dup =
[
    [ "NUnit 3.11 - October 6, 2018", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md65", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md64", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md66", null ]
    ] ],
    [ "NUnit 3.10.1 - March 12, 2018", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md67", null ],
    [ "NUnit 3.10 - March 12, 2018", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md68", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md69", null ]
    ] ],
    [ "NUnit 3.9 - November 10, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md70", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md71", null ]
    ] ],
    [ "NUnit 3.8.1 - August 28, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md72", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md73", null ]
    ] ],
    [ "NUnit 3.8 - August 27, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md74", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md75", null ]
    ] ],
    [ "NUnit 3.7.1 - June 6, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md76", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md77", null ]
    ] ],
    [ "NUnit 3.7 - May 29, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md78", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md79", null ]
    ] ],
    [ "NUnit 3.6.1 - February 26, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md80", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md81", null ]
    ] ],
    [ "NUnit 3.6 - January 9, 2017", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md82", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md83", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md84", null ]
    ] ],
    [ "NUnit 3.5 - October 3, 2016", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md85", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md86", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md87", null ]
    ] ],
    [ "NUnit 3.4.1 - June 30, 2016", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md88", [
      [ "Console Runner", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md89", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md90", null ]
    ] ],
    [ "NUnit 3.4 - June 25, 2016", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md91", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md92", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md93", null ],
      [ "Console Runner", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md94", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md95", null ]
    ] ],
    [ "NUnit 3.2.1 - April 19, 2016", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md96", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md97", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md98", null ],
      [ "Console Runner", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md99", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md100", null ]
    ] ],
    [ "NUnit 3.2 - March 5, 2016", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md101", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md102", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md103", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md104", null ]
    ] ],
    [ "NUnit 3.0.1 - December 1, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md105", [
      [ "Console Runner", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md106", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md107", null ]
    ] ],
    [ "NUnit 3.0.0 Final Release - November 15, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md108", [
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md109", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 3 - November 13, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md110", [
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md111", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md112", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 2 - November 8, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md113", [
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md114", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md115", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate - November 1, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md116", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md117", null ],
      [ "NUnitLite", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md118", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md119", null ],
      [ "Console Runner", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md120", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md121", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 5 - October 16, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md122", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md123", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md124", null ],
      [ "Console Runner", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md125", [
        [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md126", null ]
      ] ]
    ] ],
    [ "NUnit 3.0.0 Beta 4 - August 25, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md127", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md128", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md129", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md130", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 3 - July 15, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md131", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md132", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md133", null ],
      [ "Console", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md134", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md135", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 2 - May 12, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md136", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md137", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md138", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md139", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 1 - March 25, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md140", [
      [ "General", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md141", null ],
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md142", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md143", null ],
      [ "Console", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md144", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md145", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 5 - January 30, 2015", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md146", [
      [ "General", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md147", null ],
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md148", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md149", null ],
      [ "Console", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md150", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md151", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 4 - December 30, 2014", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md152", [
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md153", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md154", null ],
      [ "Console", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md155", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md156", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 3 - November 29, 2014", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md157", [
      [ "Breaking Changes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md158", null ],
      [ "Framework", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md159", null ],
      [ "Engine", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md160", null ],
      [ "Console", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md161", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md162", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 2 - November 2, 2014", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md163", [
      [ "Breaking Changes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md164", null ],
      [ "General", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md165", null ],
      [ "New Features", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md166", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md167", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 1 - September 22, 2014", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md168", [
      [ "Breaking Changes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md169", null ],
      [ "General", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md170", null ],
      [ "New Features", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md171", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md172", null ],
      [ "Console Issues Resolved (Old nunit-console project, now combined with nunit)", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md173", null ]
    ] ],
    [ "NUnit 2.9.7 - August 8, 2014", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md174", [
      [ "Breaking Changes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md175", null ],
      [ "New Features", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md176", null ],
      [ "Issues Resolved", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md177", null ]
    ] ],
    [ "NUnit 2.9.6 - October 4, 2013", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md178", [
      [ "Main Features", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md179", null ],
      [ "Bug Fixes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md180", null ],
      [ "Bug Fixes in 2.9.6 But Not Listed Here in the Release", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md181", null ]
    ] ],
    [ "NUnit 2.9.5 - July 30, 2010", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md182", [
      [ "Bug Fixes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md183", null ]
    ] ],
    [ "NUnit 2.9.4 - May 4, 2010", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md184", [
      [ "Bug Fixes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md185", null ]
    ] ],
    [ "NUnit 2.9.3 - October 26, 2009", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md186", [
      [ "Main Features", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md187", null ],
      [ "Bug Fixes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md188", null ]
    ] ],
    [ "NUnit 2.9.2 - September 19, 2009", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md189", [
      [ "Main Features", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md190", null ],
      [ "Bug Fixes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md191", null ]
    ] ],
    [ "NUnit 2.9.1 - August 27, 2009", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md192", [
      [ "General", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md193", null ],
      [ "Bug Fixes", "md__c_1__users__user__desktop__o_i_o_p9_m__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2020_1__o_i_o_p9_m_p2766e9987e11785d8279ac2be4da47f9.html#autotoc_md194", null ]
    ] ],
    [ "Felsookt", "namespace_felsookt.html", "namespace_felsookt" ]
];