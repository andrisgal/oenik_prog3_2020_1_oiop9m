var class_felsookt_1_1_logic_1_1_tests_1_1_tests =
[
    [ "EgyHallgato_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ac0660904c9ef764695fb890c65448aed", null ],
    [ "EgyKar_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ab51a6cf09482c6c9c977efe358b51802", null ],
    [ "EgySzak_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a443d10b3826a54f2143c24d8511cb059", null ],
    [ "HallgatoKeszites_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a0370b798a40d9a4fdea46c7de4c86a2d", null ],
    [ "HallgatoModositas_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a8cee19d84149e4dff16ac6c657ca5b06", null ],
    [ "HallgatoTorles_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a7e7bf8f38310106277007a537aea4bb7", null ],
    [ "Init", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ac0cb6f1a37dca56f724bf5f304c55cc9", null ],
    [ "KarKeszites_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a80455751634c3b807ae09a6b7a7316b6", null ],
    [ "KarTorles_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ade050f3e2750f1bb9f6b6490f6040dd0", null ],
    [ "LegnepszerubbKar_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a31b0999d26f9fbd29e0da4a99b3312a0", null ],
    [ "OsszesHallgato_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a12beedcb8b3ec4b7b7a51e0f30df6774", null ],
    [ "OsszesKar_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ad2d4f4e6a3809171448d06f3cb9e7f2e", null ],
    [ "OsszesSzak_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#aa6c98e6d494499be1b9d0bd7dfdeb627", null ],
    [ "SzakKeszites_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#acffa4e0f701bde07c218da048903ebf0", null ],
    [ "SzakModositas_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ab8403752a3fe11688e0cfa011762f232", null ],
    [ "SzakokAtlaga_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a74403dc0df310b6b84d521dbd34338d2", null ],
    [ "SzakokHallgatoinakSzama_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#a9a1506b73e5b464d20e21a9491e94d3e", null ],
    [ "SzakTorles_MoqTest", "class_felsookt_1_1_logic_1_1_tests_1_1_tests.html#ac47b4141d5bccfd280ad2abf126472ff", null ]
];